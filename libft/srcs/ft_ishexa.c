/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ishexa.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 18:30:26 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/03 15:32:41 by tettouat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_ishexa(char *str)
{
	if (!*str)
		return (0);
	if (*str == '0' && str[1] == 'x')
		str += 2;
	while (*str)
	{
		if (!ft_isxdigit(*str))
			return (0);
		++str;
	}
	return (1);
}
