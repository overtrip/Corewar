/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ahtoi_limits.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/03 15:20:23 by tettouat          #+#    #+#             */
/*   Updated: 2017/05/10 17:25:57 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	s_is_specialchar(int c)
{
	if (c == '\n'
			|| c == '\v'
			|| c == '\t'
			|| c == '\r'
			|| c == '\f'
			|| c == ' ')
		return (1);
	return (0);
}

static int	s_is_alpha(int c)
{
	if (c >= 'a' && c <= 'f')
		return (1);
	if (c >= 'A' && c <= 'F')
		return (1);
	return (0);
}

static int	s_to_int(const char *s, int is_neg, char *flag)
{
	unsigned int		ret;

	ret = 0;
	while (ft_isdigit(*s) || s_is_alpha(*s))
	{
		if (*s >= 'a' && *s <= 'f')
			ret = ret * 16 + *s - 'a' + 10;
		else if (*s >= 'A' && *s <= 'F')
			ret = ret * 16 + *s - 'A' + 10;
		else
			ret = ret * 16 + *s - '0';
		++s;
	}
	if (ret > INT_MAX || ret < INT_MIN)
	{
		*flag |= 1;
		return (0);
	}
	return ((int)ret * is_neg);
}

int			ft_ahtoi_limits(const char *s, char *flag)
{
	int		is_neg;

	if (!s || !*s)
		return (0);
	is_neg = 1;
	while (s_is_specialchar(*s))
		++s;
	while ((*s == '+' || *s == '-') && is_neg == 1)
	{
		if (*s == '-' || *(s + 1) == '+')
		{
			is_neg = -1;
			if (*(s - 1) == '+')
				--s;
		}
		++s;
	}
	if (*s == '0' && *(s + 1) == 'x')
		s += 2;
	else if (*s == '#')
		++s;
	return (s_to_int(s, is_neg, flag));
}
