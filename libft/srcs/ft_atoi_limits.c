/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_limits.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/03 15:12:42 by tettouat          #+#    #+#             */
/*   Updated: 2017/05/10 17:43:15 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_searchchr(int c)
{
	char	*buf;
	int		i;

	buf = "\n\v\t\r\f ";
	i = 0;
	while (i < 6)
	{
		if (buf[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int			ft_atoi_limits(const char *str, char *flag)
{
	unsigned int	result;
	int				is_neg;

	result = 0;
	is_neg = 1;
	printf("--[%s]--\n", str);
	while (ft_searchchr(*str))
		str++;
	while ((*str == '+' || *str == '-') && is_neg == 1)
	{
		if ((*str == '-' || *(str + 1) == '+') && (is_neg = -1))
			if (*(str - 1) == '+')
				str--;
		str++;
	}
	while (*str && ft_isdigit(*str))
	{
		result = result * 10 + *str - '0';
		str++;
	}
	printf("[%d](int find)[%d][%d]\n", result, INT_MIN, INT_MAX);
	if (result > 2147483647)
	{
		printf("[ICI][%d](in if condition)\n", result);
		*flag |= 1;
		return (0);
	}
	return (result * is_neg);
}
