/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_instruction.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 18:00:17 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 19:19:36 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Return the string without label if they are on same line
*/

char			*return_without_label(char *str)
{
	char	*end;
	char	*begin;
	char	*tmp;

	begin = str;
	if (!(end = ft_strchr(begin, LABEL_CHAR)))
		return (ft_strdup(begin));
	tmp = ft_strndup(begin, end - begin);
	if (!is_t_lab(tmp))
		return (ft_strdup(begin));
	begin = ft_strtrim(end + 1);
	free(tmp);
	return (begin);
}

/*
**	Search in op.c the corespondence
*/

static int		search_instruction(char *str, int line)
{
	int			i;
	char		*to_find;
	char		*begin;
	char		*without_label;
	extern t_op	g_op_tab[SIZE];

	if ((i = -1) && !(str && *str))
		return (-1);
	without_label = return_without_label(str);
	begin = without_label;
	to_find = ft_strchr(without_label, ' ');
	if (to_find && *to_find)
		begin = ft_strndup(without_label, to_find - begin);
	while (++i < SIZE)
	{
		if (!ft_strcmp(begin, g_op_tab[i].name))
		{
			free(begin);
			return (i);
		}
	}
	if (!(*begin == ';' || *begin == '#'))
		send_id("token", line);
	return (-1);
}

/*
**	filing struct with elements
*/

void			fill_value_instruction(t_head *head, int i, char *str)
{
	t_instruct	*tmp;
	char		*cut;
	char		*trim;
	char		*save;

	tmp = (t_instruct *)head->last->data;
	if (!(str || *str))
		return ;
	cut = cut_comment(str);
	trim = ft_strtrim(cut);
	save = NULL;
	if (trim)
	{
		if (tmp->arg_type[i] == DIR_CODE)
		{
			if ((save = ft_strchr(trim, ':')))
				tmp->arg_value[i] = ft_strdup(save + 1);
			else
				tmp->arg_value[i] = ft_strdup(trim);
		}
		else
			tmp->arg_value[i] = ft_strdup(trim);
		free(trim);
		free(cut);
	}
}

/*
**	Check if line is a comment or a label
*/

static int		go_out(char *str)
{
	char	*find;

	if (*str == '#')
		return (1);
	if ((find = ft_strrchr(str, ':')))
	{
		if (ft_strrchr(str, '%'))
			return (0);
		else
			return (1);
	}
	return (0);
}

/*
**	Check if  instructions are valide
*/

int				find_instruction(char **cast, t_posandflag *var, t_head *head)
{
	char		*new;
	t_moche		p;
	void		*save;
	extern t_op	g_op_tab[SIZE];

	*cast = return_without_label(*cast);
	save = *cast;
	new = ft_strtrim(save);
	if (new && *new && !go_out(new))
	{
		p.index = search_instruction(new, var->line);
		if (!(p.i = 0) && p.index > -1 && p.index < SIZE)
		{
			create_instruction(head, p.index, cast, var);
			if (g_op_tab[p.index].nb_arg > 1)
				var->pos += 1;
			while (p.i < g_op_tab[p.index].nb_arg)
				if (sequel(var, cast, head, &p) &&
						bad_arg(p.i++, p.index, head, var))
					return (1);
		}
	}
	*cast = save;
	free(new);
	return (0);
}
