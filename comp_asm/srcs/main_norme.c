/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_norme.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:51:37 by tettouat          #+#    #+#             */
/*   Updated: 2017/04/21 14:52:01 by tettouat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Create to cut str[0] and free str[1] avoid some leaks
*/

void		stock_box(char ***box, char *str)
{
	if (!(*box = (char **)malloc(sizeof(char *) * 2)))
		return ;
	(*box)[0] = str;
	(*box)[1] = str;
}

/*
**	Replace tab character by a space
*/

void		replace_char(char *str)
{
	int i;

	i = 0;
	if (!str)
		return ;
	while (str[i])
	{
		if (str[i] == '\t')
			str[i] = ' ';
		++i;
	}
}
