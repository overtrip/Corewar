/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_value.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 15:30:25 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/13 18:09:36 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	If argument is a label return position value
*/

static int	is_label(t_lists *label, char *arg, size_t size)
{
	t_lists	*current;
	t_order	*tmp;

	current = label;
	while (current)
	{
		tmp = (t_order *)current->data;
		if (*arg == ':')
			++arg;
		if (!ft_strcmp(arg, tmp->label))
		{
			if ((int)(tmp->pos - size) < 0)
				return ((int)(tmp->pos - size) + 1);
			return ((int)(tmp->pos - size));
		}
		current = current->next;
	}
	if (*arg == '%')
		++arg;
	return (size_and_chars(arg));
}

/*
**	Return if  argument is a T_DIT or T_REG
*/

static int	select_value(t_instruct *arg, int index, t_lists *label,
																t_moche *st)
{
	char	*buff;

	buff = arg->arg_value[index];
	if (arg->arg_type[index] == REG_CODE)
		++buff;
	return (is_label(label, buff, (st->cmp - st->decal)));
}

/*
**	write arguments in file
*/

int			true_value(t_instruct *arg, t_lists *label, int index, t_moche *st)
{
	char			*buff;
	int				res;
	int				c;

	buff = NULL;
	if (arg->arg_type[index] != IND_CODE)
		return (select_value(arg, index, label, st));
	if (!ft_isdecimal(arg->arg_value[index]))
	{
		res = is_in_limit(arg, index);
		st->index = res >> 8;
		st->i = res << 8;
		res = st->i + st->index;
	}
	else
	{
		res = is_in_limit(arg, index);
		st->index = res & 0xff;
		st->i = res & 0xff00;
		c = res & 0xffff0000;
		st->index = st->index << 8;
		st->i = st->i >> 8;
		res = c + st->index + st->i;
	}
	return (res);
}
