/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_int_size.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jealonso <jealonso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/11 15:23:08 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/13 18:09:38 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Return 1 if is inside limits and 0 if not to a decimal number
*/

int	check_loop(char *str, char *ref_max)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == ref_max[i])
			++i;
		else if (str[i] > ref_max[i])
			return (0);
		else
			return (1);
	}
	return (1);
}

/*
**	Return 1 if is inside limits and 0 if not to a hexdecimal number
*/

int	loop_hexa(char *str)
{
	int		i;
	char	*tmp;

	i = 0;
	tmp = ft_strchr(str, 'x');
	++tmp;
	while (str[i])
	{
		if (str[i] == 'f')
			++i;
		else if (str[i] > 'f')
			return (0);
		else
			return (1);
	}
	return (1);
}

/*
**	Make a loop to compare chars by char if the int is an overflow
*/

int	loop_to_compare(char *str)
{
	char	*ref_max;
	char	*ref_min;

	ref_max = ft_itoa(INT_MAX);
	ref_min = ft_itoa(INT_MIN);
	if (ft_isdecimal(str))
	{
		if (*str == '-')
			return (check_loop(str, ref_min));
		return (check_loop(str, ref_max));
	}
	else
		return (loop_hexa(str));
}
