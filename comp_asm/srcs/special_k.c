/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   special_k.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jealonso <jealonso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/13 15:02:52 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/13 18:01:44 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Return an integer if the string was upper the limit
*/

int	return_limit(int decimal, char *str)
{
	if (decimal && (*str == '-'))
		return (INT_MIN);
	return (INT_MAX);
}

/*
**	Return the value number if the string is compose of less ten chars
*/

int	return_true_int(int decimal, char *str)
{
	if (decimal)
		return (ft_atoi(str));
	return (ft_ahtoi(str));
}
