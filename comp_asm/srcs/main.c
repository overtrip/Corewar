/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 17:22:15 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 17:56:26 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Dup the buffer, create and push back the new link
*/

static int	get_line(t_head *champ, char **buff, int line)
{
	t_lists	*new_line;
	char	*tmp_buff;
	char	**box;

	box = NULL;
	if (!(tmp_buff = ft_strdup(*buff)))
		return (send_id("no_maloc_data", line));
	replace_char(tmp_buff);
	ft_strdel(buff);
	stock_box(&box, tmp_buff);
	if (!(new_line = ft_lst_create_no_malloc(box)))
		return (send_id("no_malloc_link", line));
	if (!ft_lst_push_back(&champ, new_line))
		return (send_id("no_malloc_link", line));
	return (0);
}

/*
**	Initialise all to 0 or NULL
*/

static void	init_null(t_head *files, int *line, t_moche *var, char *file_name)
{
	files->first = NULL;
	files->last = NULL;
	var->cmp = 0;
	var->decal = 0;
	var->prog_size = 0;
	var->file_name = file_name;
	line = 0;
}

/*
**	Calling to open and close files
*/

static int	open_files(char *file_name)
{
	int		res_open;
	int		line;
	char	*buff;
	t_head	champ;
	t_moche	*var;

	if (!(var = (t_moche *)malloc(sizeof(t_moche))))
		return (1);
	init_null(&champ, &line, var, file_name);
	if ((res_open = open(file_name, O_RDONLY)) < 0)
		return (send_id("open", 0));
	else
	{
		while (get_next_line(res_open, &buff) > 0 && ++line)
		{
			if (get_line(&champ, &buff, line))
				var->cmp = 1;
		}
		if (check_content(champ.first, var))
			var->cmp = 1;
	}
	if (close(res_open) < 0)
		return (send_id("close", 0));
	return (var->cmp);
}

/*
**	You know that
*/

int			main(int argc, char **argv)
{
	int	i;
	int ret;

	i = 0;
	ret = 0;
	if (argc < 2 || (argc - 1) > MAX_ARGS_NUMBER)
		return (send_id("usage", 0));
	while (++i < argc)
		if (!open_files(argv[i]))
			ret++;
	if (ret)
		ft_putendl_fd("ERROR: There is a bad champion in the list.", 2);
	return (0);
}
