/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_content.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:44:45 by jealonso          #+#    #+#             */
/*   Updated: 2017/04/20 15:25:27 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Return value of all instruction args
*/

char		value_arg(t_instruct *save)
{
	int				i;
	unsigned char	res;
	extern t_op		g_op_tab[SIZE];

	i = 0;
	res = 0;
	while (i < g_op_tab[save->op_code - 1].nb_arg)
	{
		res += (unsigned char)save->arg_type[i];
		res <<= 2;
		++i;
	}
	while (i < 3)
	{
		res <<= 2;
		++i;
	}
	return (res);
}

/*
**	Return the size of arg_value
*/

size_t		size_value(t_instruct *current, int i)
{
	extern t_op	g_op_tab[SIZE];
	int			var;

	var = current->arg_type[i];
	if (var == DIR_CODE)
		return (!g_op_tab[current->op_code - 1].label_size ? 4 : 2);
	if (var == REG_CODE)
		return (1);
	if (var == IND_CODE)
		return (2);
	return (0);
}

void		swap_int(int *value, t_instruct *save)
{
	int			save_1;
	int			save_2;
	extern t_op	g_op_tab[SIZE];

	if (g_op_tab[save->op_code - 1].label_size)
	{
		save_1 = *value >> 8;
		save_2 = *value << 8;
		*value = save_1 + save_2;
	}
	else
		swapped((unsigned int *)value);
}

/*
**	Write agrs value and args
*/

void		write_args(t_lists *save, t_lists *label, int fd, t_moche *st)
{
	int			i;
	int			nb_args;
	int			tmp_2;
	char		tmp_1;
	extern t_op g_op_tab[SIZE];

	i = 0;
	nb_args = g_op_tab[DATA->op_code - 1].nb_arg;
	if (g_op_tab[DATA->op_code - 1].octet_code == 1)
	{
		tmp_1 = value_arg(DATA);
		write(fd, &tmp_1, sizeof(char));
		st->cmp += 1;
		++st->decal;
	}
	while (i < nb_args)
	{
		++st->decal;
		tmp_2 = true_value(DATA, label, i, st);
		if (DATA->arg_type[i] == DIR_CODE)
			swap_int(&tmp_2, DATA);
		write(fd, &tmp_2, size_value(DATA, i));
		st->cmp += size_value(DATA, i);
		++i;
	}
}

/*
**	Write instructions
*/

void		write_instruction(t_head *order_list, t_head *label_pos, int fd,
																	t_moche *st)
{
	t_lists *label;
	t_lists	*save;

	label = label_pos->first;
	save = order_list->first;
	st->cmp = 0;
	while (save)
	{
		write(fd, &DATA->op_code, sizeof(char));
		st->cmp += 1;
		st->decal = 0;
		write_args(save, label, fd, st);
		save = save->next;
	}
}
