/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_message.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 14:34:34 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 17:54:35 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Print message when T_DIR arguments isn't an hexadecimal
*/

int		error_hexa(int line, char *str)
{
	ft_putstr("ERROR: In line [");
	ft_putnbr(line);
	ft_putstr("], the argument [");
	ft_putstr(str);
	ft_putendl("] is not an hexadecimal number.");
	return (1);
}

/*
**	Error message is print when a T_REG issn't between REG_NUMBER
*/

int		error_reg_message(int value, int line)
{
	ft_putstr("ERROR: In line [");
	ft_putnbr(line);
	ft_putstr("] use a T_REG [");
	ft_putnbr(value);
	ft_putstr("] who's no between [0] and [");
	ft_putnbr(REG_NUMBER);
	ft_putendl("].");
	return (1);
}

/*
**	Print message to error arg token
*/

void	error_token(int index, int arg, int i, int line)
{
	char	**str;

	str = NULL;
	ft_putstr("ERROR token: Your argument is of type [");
	ft_putstr(ret_value(arg));
	ft_putstr("] must be of type [");
	ft_putstr(ret_possible(index, str, i));
	free(str);
	ft_putstr("] at line [");
	ft_putnbr(line);
	ft_putendl("].");
}

/*
** 	Print meassage when the arg value is under limits
*/

int		error_int_size(char *str, int line)
{
	ft_putstr("ERROR: In line [");
	ft_putnbr(line);
	ft_putstr("], the argument [");
	ft_putstr(str);
	ft_putstr("] is under INT values. It must be between [");
	ft_putnbr(INT_MIN);
	ft_putstr("] and [");
	ft_putnbr(INT_MAX);
	ft_putendl("].");
	return (1);
}

/*
**	Print message if champion size is excedeed
*/

int		error_prog_size(int size, t_head elem)
{
	ft_putstr("ERROR Champion size is out of range of [");
	ft_putnbr(size - CHAMP_MAX_SIZE);
	ft_putstr("] it must be [");
	ft_putnbr(CHAMP_MAX_SIZE);
	ft_putendl("] bytes maximum.");
	delete_label(elem.first);
	return (-1);
}
