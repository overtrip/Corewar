/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bad_arg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 14:34:34 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 18:49:21 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Check the int and return if is under limits
*/

int		size_and_chars(char *str)
{
	int	check;
	int	decimal;
	int	res;

	decimal = ft_isdecimal(str);
	if (*str == '-')
		check = ft_strlen(str) - 1;
	else
		check = ft_strlen(str);
	if (check > 10)
		return (return_limit(decimal, str));
	res = loop_to_compare(str);
	if (check < 10 || res)
		return (return_true_int(decimal, str));
	else if (!res)
		if (*str == '-')
			return (INT_MIN);
	return (INT_MAX);
}

/*
** Check if the value who's sent is in int limits
*/

int		is_in_limit(t_instruct *arg, int i)
{
	int		ret;

	ret = 0;
	if (arg->arg_type[i] != REG_CODE)
	{
		if (arg->arg_type[i] == DIR_CODE && ft_strchr(arg->arg_value[i], '%'))
			ret = size_and_chars(arg->arg_value[i] + 1);
		if (arg->arg_type[i] == IND_CODE)
			ret = size_and_chars(arg->arg_value[i]);
	}
	return (ret);
}

/*
**	Return final string to print args message
*/

char	*final_string(char **str, int counter)
{
	char	*save;

	save = NULL;
	save = ft_strdup(str[counter]);
	counter--;
	while (counter > -1)
	{
		save = ft_strjoin(save, str[counter]);
		free(str[counter]);
		--counter;
	}
	return (save);
}

/*
**	Check if the argument T_RG is between its max values
*/

int		error_reg_value(t_instruct *arg, int cmp, int line)
{
	int		value;

	value = 0;
	if (arg->arg_type[cmp] == REG_CODE)
	{
		value = ft_atoi(arg->arg_value[cmp] + 1);
		if (value > REG_NUMBER || value < 0)
			return (error_reg_message(value, line));
	}
	return (0);
}

/*
**	Compare current arg if it's possible
*/

int		bad_arg(int cmp, int index, t_head *head, t_posandflag *var)
{
	extern t_op		g_op_tab[SIZE];
	t_instruct		*tmp;
	int				to_compare;

	tmp = (t_instruct *)head->last->data;
	to_compare = ret_type(tmp->arg_type[cmp]);
	if (error_reg_value(tmp, cmp, var->line))
		return (1);
	if (tmp->arg_type[cmp] == DIR_CODE && ft_strchr(tmp->arg_value[cmp], '%'))
	{
		if (!ft_isnumber(tmp->arg_value[cmp] + 1))
			return (error_hexa(var->line, tmp->arg_value[cmp]));
	}
	if ((g_op_tab[index].arg_t[cmp] & to_compare) == to_compare)
		return (0);
	if (!error_nb_arg(tmp, index, var->line))
		error_token(index, to_compare, cmp, var->line);
	return (1);
}
