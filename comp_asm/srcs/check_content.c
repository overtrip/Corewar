/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_content.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/17 14:31:12 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 17:54:58 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Delete all label list elements
*/

void				delete_label(t_lists *elem)
{
	t_lists	*save;
	t_lists	*current;

	if (!elem)
		return ;
	current = elem;
	while (current)
	{
		save = current;
		current = current->next;
		if (((t_order *)(save->data))->label)
			free(((t_order *)(save->data))->label);
		free(save->data);
		free(save);
	}
}

/*
**	Initialise check_content vars
*/

static t_lists		*init_vars(t_head *head, t_head *label_pos,
	t_posandflag *var, t_lists *champ)
{
	ft_bzero(label_pos, sizeof(t_head));
	ft_bzero(head, sizeof(t_head));
	ft_bzero(var, sizeof(t_posandflag));
	var->pos = 0;
	var->flag = 0;
	return (champ);
}

/*
**	Launching tests to look the structure construction
*/

int					check_content(t_lists *champ, t_moche *st)
{
	t_lists			*cpy;
	t_posandflag	var;
	t_head			head;
	t_head			label_pos;
	char			*cast;

	cpy = init_vars(&head, &label_pos, &var, champ);
	while (cpy && ++var.line)
	{
		cast = (char*)((char **)cpy->data)[0];
		if (find_prerequis(cast, &var.flag, var.line))
			return (1);
		if (!find_label(&var.flag, var.line))
			find_pos_label(&cast, &var.pos, &label_pos);
		if (find_flag_inst(&var.flag))
			if (find_instruction(cpy->data, &var, &head))
				return (1);
		cpy = cpy->next;
	}
	st->prog_size = &var.pos;
	if (var.pos > CHAMP_MAX_SIZE)
		return (error_prog_size(var.pos, label_pos));
	open_new_file(st, &head, &label_pos, champ);
	delete_label(label_pos.first);
	return (0);
}
