/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_instruction_norme.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jealonso <jealonso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 18:20:38 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 19:46:39 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Cut comment and trim string
*/

char		*cut_comment(char *str)
{
	char	*delete;
	char	*cut;
	char	*tmp;
	char	*ret;

	delete = NULL;
	cut = NULL;
	tmp = NULL;
	ret = NULL;
	if ((delete = ft_strrchr(str, ';'))
		|| (delete = ft_strchr(str, '#')))
	{
		tmp = ft_strsub(str, 0, delete - str);
		cut = ft_strtrim(tmp);
		free(tmp);
		return (cut);
	}
	if (str && *str)
		ret = ft_strtrim(str);
	return (ret);
}

/*
**	filing struct with elements
*/

static void	fill_type_instruction(t_head *head, int i, char *str,
															unsigned int *pos)
{
	t_instruct	*tmp;
	char		type;
	char		*cut;

	tmp = (t_instruct *)head->last->data;
	if (!(str || *str))
		return ;
	cut = cut_comment(str);
	type = define_type(cut);
	free(cut);
	if (type)
	{
		tmp->arg_type[i] = type;
		*pos += size_arg(type, (int)(tmp)->op_code);
	}
}

/*
**	Return an error when there are to many arguments
*/

int			error_to_many_arg(char *substring, int line)
{
	ft_putstr("ERROR: In line [");
	ft_putnbr(line);
	ft_putstr("] the string [");
	ft_putstr(substring);
	ft_putendl("]contain too many arguments.");
	exit(1);
}

/*
**	Count many comma
*/

int			nb_comma(char *str, int i)
{
	char		*tmp;
	int			cmp;
	extern t_op	g_op_tab[SIZE];

	tmp = str;
	cmp = 0;
	while ((tmp = ft_strchr(tmp, ',')))
	{
		if (*tmp == ',')
			++tmp;
		++cmp;
		if (cmp == g_op_tab[i].nb_arg)
			return (1);
	}
	return (0);
}

/*
**	Create for the norme to be correct
*/

int			sequel(t_posandflag *var, char **cast, t_head *head, t_moche *i)
{
	char		*substring;
	t_instruct	*tmp;
	extern t_op	g_op_tab[SIZE];

	tmp = (t_instruct *)head->last->data;
	substring = NULL;
	if (nb_comma(*cast, i->index))
		return (error_to_many_arg(*cast, var->line));
	substring = parse_strsep(cast, ",");
	if (!substring)
		return (1);
	fill_type_instruction(head, i->i, substring, &var->pos);
	fill_value_instruction(head, i->i, substring);
	ft_strdel(&substring);
	return (1);
}
