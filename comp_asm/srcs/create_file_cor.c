/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_file_cor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/26 16:09:52 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 14:28:58 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Initialise header of champ
*/

static void		init_head(t_header *head, unsigned int *size)
{
	ft_bzero(head, sizeof(t_header));
	head->magic = COREWAR_EXEC_MAGIC;
	swapped(&head->magic);
	head->prog_size = *size;
	swapped(&head->prog_size);
}

/*
**	change name to create a writed in file.cor
*/

static char		*change_name(char *file_name)
{
	char	*final_path;
	char	*name;

	name = NULL;
	final_path = NULL;
	if (!(final_path = ft_strrchr(file_name, '.')))
		return (NULL);
	name = ft_strndup(file_name, final_path - file_name);
	final_path = ft_strjoin(name, ".cor");
	free(name);
	return (final_path);
}

/*
**	Write in file all values converted
*/

void			write_header(t_lists *champ, int res_open, t_moche *st)
{
	t_lists			*cpy;
	t_header		head;
	int				line;
	char			*cast;
	t_head			*label_list;

	cpy = champ;
	line = 0;
	label_list = NULL;
	init_head(&head, st->prog_size);
	while (cpy)
	{
		cast = (char*)((char **)cpy->data)[0];
		if (!ft_strncmp(cast, NAME_CMD_STRING, ft_strlen(NAME_CMD_STRING)))
			write_name(cast, &head);
		if (!ft_strncmp(cast, COMMENT_CMD_STRING,
					ft_strlen(COMMENT_CMD_STRING)))
			write_comment(cast, &head);
		cpy = cpy->next;
	}
	print_header(head, res_open);
}

/*
**	check if declaration label exist
*/

int				check_list(t_instruct *list, t_head *label_list)
{
	extern t_op		g_op_tab[SIZE];
	t_lists			*label;
	char			*save;
	int				i;

	label = label_list->first;
	save = NULL;
	i = -1;
	while (++i < g_op_tab[list->op_code - 1].nb_arg)
	{
		save = list->arg_value[i];
		if (if_label_find(label, list, i))
			return (error_arg_label(label_list->first, save));
	}
	return (0);
}

/*
**	Create, open, call a writen function and close the new file.cor
*/

int				open_new_file(t_moche *st, t_head *head, t_head *label_pos,
		t_lists *champ)
{
	int				res_open;

	st->file_name = change_name(st->file_name);
	if (error_calling_label(head, label_pos))
		return (-1);
	if ((res_open = open(st->file_name, O_RDWR | O_CREAT | O_TRUNC, 0755)) < 0)
		send_id("", 0);
	write_header(champ, res_open, st);
	write_instruction(head, label_pos, res_open, st);
	if (close(res_open) < 0)
		return (-2);
	free(st->file_name);
	return (0);
}
