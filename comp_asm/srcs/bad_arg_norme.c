/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bad_arg_norme.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:41:51 by tettouat          #+#    #+#             */
/*   Updated: 2017/04/26 16:52:47 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Return type arg (using in error_token to write message)
*/

char	*ret_value(int value)
{
	if (value == T_IND)
		return ("T_IND");
	if (value == T_DIR)
		return ("T_DIR");
	if (value == T_REG)
		return ("T_REG");
	return (NULL);
}

/*
**	Count how much args possibilities
*/

int		ret_count(int cmp, int index, int i)
{
	int				counter;
	extern	t_op	g_op_tab[SIZE];

	counter = 0;
	while (cmp < T_LAB)
	{
		if (g_op_tab[index].arg_t[i] & cmp)
			++counter;
		cmp += cmp;
	}
	return (counter);
}

/*
**	Return all possibility args
*/

char	*ret_possible(int index, char **str, int i)
{
	int				cmp;
	int				temp;
	int				counter;
	char			*tmp;
	extern	t_op	g_op_tab[SIZE];

	cmp = 1;
	counter = -1;
	temp = ret_count(cmp, index, i);
	if (!(str = (char **)malloc(sizeof(char *) * temp + 1)))
		return (NULL);
	while (cmp < T_LAB)
	{
		if ((g_op_tab[index].arg_t[i] & cmp))
		{
			++counter;
			tmp = ret_value(g_op_tab[index].arg_t[i] & cmp);
			if (counter == 0)
				str[counter] = ft_strdup(tmp);
			else
				str[counter] = ft_strjoin(tmp, " | ");
		}
		cmp += cmp;
	}
	return (final_string(str, counter));
}

/*
**	return arg_type value to type
*/

int		ret_type(int value)
{
	if (value == IND_CODE)
		return (4);
	if (value == DIR_CODE)
		return (2);
	if (value == REG_CODE)
		return (1);
	return (8);
}

/*
**	Return if T_DIR isn't an hexadecimal number
*/

int		not_an_hexa(char *str)
{
	char *save;

	save = str;
	++save;
	if (ft_ishexa(save))
		return (0);
	return (1);
}
