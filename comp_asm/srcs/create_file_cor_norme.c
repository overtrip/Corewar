/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_file_cor_norme.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 14:55:02 by tettouat          #+#    #+#             */
/*   Updated: 2017/05/22 20:05:42 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Swap int to little edian
*/

void			swapped(unsigned int *num)
{
	unsigned int	swapped;

	swapped = ((*num >> 24) & 0xff) |
	((*num << 8) & 0xff0000) |
	((*num >> 8) & 0xff00) |
	((*num << 24) & 0xff000000);
	*num = swapped;
}

/*
**	Check if label arg calling exist
*/

int				error_calling_label(t_head *instruction, t_head *label_list)
{
	t_lists	*head;

	head = instruction->first;
	while (head)
	{
		if (check_list(head->data, label_list))
			return (1);
		head = head->next;
	}
	return (0);
}

/*
**	Check if argument label is OK
*/

int				if_label_find(t_lists *label, t_instruct *list, int i)
{
	t_order	*tmp;

	if (list->arg_type[i] != DIR_CODE)
		return (0);
	if (list->arg_type[i] == DIR_CODE &&
			ft_isnumber(list->arg_value[i] + 1))
		return (0);
	if (list->arg_type[i] == DIR_CODE)
	{
		while (label)
		{
			tmp = (t_order *)(label->data);
			if (!ft_strcmp(tmp->label, list->arg_value[i]))
				return (0);
			label = label->next;
		}
	}
	if (list->arg_type[i] == IND_CODE)
	{
		if (!ft_strcmp(tmp->label, list->arg_value[i]))
			return (0);
		if (ft_isnumber(list->arg_value[i]))
			return (0);
	}
	return (1);
}
