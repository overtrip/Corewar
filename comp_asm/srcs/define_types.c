/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define_types.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jealonso <jealonso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 13:29:44 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 14:50:18 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Check if char is a label_char
*/

int			is_t_lab(char *str)
{
	char	*tmp;
	char	*current;

	tmp = LABEL_CHARS;
	current = str;
	if (!str || !*str)
		return (0);
	while (*current)
	{
		if (!ft_strchr(tmp, *current))
			return (0);
		++current;
	}
	return (1);
}

/*
**	Check if the string was a T_REG
*/

static int	is_t_reg(char *str)
{
	int		count;
	int		result;

	count = ft_strlen(str);
	result = 0;
	if (ft_isnumber(str))
		result = ft_atoi(str);
	return (result);
}

/*
**	Define type of arg
*/

char		define_type(char *str)
{
	int	i;

	i = -1;
	if (!str || !*str)
		return (0);
	if (*str == '%')
		return (DIR_CODE);
	else if (*str == 'r' && is_t_reg(str + 1))
		return (REG_CODE);
	if (*str && ft_isnumber(str))
		return (IND_CODE);
	if (is_t_lab(str))
		return (T_LAB);
	return (0);
}

/*
**	Check if T_DIR size is 2 or 4 octets
*/

int			size_arg(char value, int op_code)
{
	unsigned int	size;
	extern t_op		g_op_tab[SIZE];

	size = g_op_tab[op_code - 1].label_size;
	if (value == DIR_CODE)
		return (size == 0 ? 4 : 2);
	if (value == REG_CODE)
		return (1);
	if (value == IND_CODE)
		return (2);
	return (0);
}
