/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_nb_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jealonso <jealonso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 20:01:20 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 19:44:04 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**	Prints when the label_arg is called but doesn't exist in the list labels
*/

int			error_arg_label(t_lists *list, char *str)
{
	t_lists	*label;
	t_order	*tmp;

	label = list;
	ft_putstr("ERROR: Argument label [");
	ft_putstr(str);
	ft_putstr("] is called but there is no label with this name.");
	ft_putendl("You can call :");
	while (label)
	{
		tmp = (t_order *)(label->data);
		ft_putchar('[');
		ft_putstr(tmp->label);
		ft_putendl("]");
		label = label->next;
	}
	return (1);
}

/*
**	Print error message in error_nb_arg function
*/

static int	print_error_nb_arg(int i, int line)
{
	extern t_op	g_op_tab[SIZE];

	ft_putstr("ERROR: In fonction [");
	ft_putstr(g_op_tab[i].name);
	ft_putstr("] in line [");
	ft_putnbr(line);
	ft_putendl("] provided arguments are invalid.");
	return (1);
}

/*
** Return an error if the number of argument is invalid
*/

int			error_nb_arg(t_instruct *arg, int index, int line)
{
	int			cmp;
	extern t_op	g_op_tab[SIZE];

	cmp = -1;
	while (++cmp < g_op_tab[index].nb_arg)
		if (!(arg->arg_value[cmp]) && cmp < g_op_tab[index].nb_arg)
			return (print_error_nb_arg(index, line));
	return (0);
}
