/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/05 15:13:40 by jealonso          #+#    #+#             */
/*   Updated: 2017/05/23 17:54:45 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H

# include "libft.h"
# include "op.h"
# include <fcntl.h>
# include <limits.h>

/*
**	TODO To delete after tests because used to printf function
*/

# include <stdio.h>

/*
**	Use for define size of errors message in (t_mess)
*/

# define INDEX			21

/*
**	Use to define who much function op_t contain
*/

# define SIZE			17

/*
**	Use to find if fiels exist and their formats
*/

# define FLAG_NAME		1
# define FLAG_COMMENT	2
# define FLAG_LABEL		4
# define FLAG_INST		8
# define FLAG_AFTER		16

/*
**	Macro for economized lines
*/

# define FLAG_N(flag)	(flag & FLAG_NAME)
# define FLAG_C(flag)	(flag & FLAG_COMMENT)
# define DATA ((t_instruct *)(save)->data)

/*
**	Define a strusture who contain all error massage
*/

typedef struct			s_mess
{
	char				*key_word;
	char				*message;
}						t_mess;

typedef struct			s_order
{
	unsigned int		pos;
	char				*label;
	struct s_order		*next;
}						t_order;

typedef struct			s_instruct
{
	int					op_code;
	int					*arg_type;
	char				**arg_value;
	struct s_instruct	*next;
}						t_instruct;

/*
**	Struct to economized lines in check_content.c
*/

typedef struct			s_posandflag
{
	int					line;
	unsigned int		pos;
	unsigned char		flag;
}						t_posandflag;

/*
**	Struct to get proz_size in check_content.c and to 42 norme
*/

typedef struct			s_moche
{
	char				*file_name;
	unsigned int		*prog_size;
	size_t				cmp;
	size_t				decal;
	int					index;
	int					i;
}						t_moche;

int						bad_arg(int i, int index, t_head *head, t_posandflag
		*var);
int						check_content(t_lists *champ, t_moche *var);
int						check_list(t_instruct *list, t_head *label_list);
void					create_instruction(t_head *head, int index, char **str,
															t_posandflag *var);
char					*cut_comment(char *str);
char					define_type(char *str);
void					delete_label(t_lists *elem);
int						error_arg_label(t_lists *list, char *str);
int						error_calling_label(t_head *instruction,
		t_head *label_list);
int						error_hexa(int	line, char *str);
int						error_int_size(char *str, int line);
int						error_nb_arg(t_instruct *arg, int index, int line);
int						error_prog_size(int size, t_head elem);
int						error_reg_message(int value, int line);
void					error_token(int index, int arg, int i, int line);
void					fill_value_instruction(t_head *head, int i, char *str);
char					*final_string(char **str, int counter);
int						find_flag_inst(unsigned char *flag);
int						find_instruction(char **data, t_posandflag *var,
																t_head *head);
int						find_label(unsigned char *flag, int line);
void					find_pos_label(char	**data, unsigned int *size,
		t_head *pos);
int						find_prerequis(char *data, unsigned char *flag,
		int line);
int						if_label_find(t_lists *label, t_instruct *list, int i);
int						is_in_limit(t_instruct *arg, int i);
int						is_t_lab(char *str);
int						loop_to_compare(char *str);
int						not_an_hexa(char *str);
int						open_new_file(t_moche *st, t_head *head,
		t_head *label_pos, t_lists *champ);
char					*parse_strsep(char **str, const char *delim);
void					print_header(t_header head, int res_open);
void					replace_char(char *str);
char					*ret_possible(int index, char **str, int i);
int						ret_type(int value);
char					*ret_value(int value);
int						return_limit(int decimal, char *str);
int						return_true_int(int decimal, char *str);
int						send_id(char *str, int line);
int						sequel(t_posandflag *var, char **cast, t_head *head,
		t_moche *i);
int						size_and_chars(char *str);
int						size_arg(char value, int op_code);
void					stock_box(char ***box, char *str);
void					swap_int(int *value, t_instruct *save);
void					swapped(unsigned int *num);
int						true_value(t_instruct *arg, t_lists *label, int index,
		t_moche *st);
void					write_comment(char *data, t_header *head);
void					write_header(t_lists *champ, int res_open, t_moche *st);
void					write_instruction(t_head *order_list,
		t_head *label_pos, int fd, t_moche *st);
void					write_name(char *data, t_header *head);

/*
**	Use to debuging in debug.c
*/

void					print_champ_data(t_head *champ);
void					print_content(char **tab);
void					print_label(t_head *label);
void					print_instruction(t_head *instruction);

#endif
