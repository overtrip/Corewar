.name "simple"
.comment "I'm so stupid!"

begin:
	lfork	%2048
	sti		r1, %:wall, %1
	sti		r1, %:loop, %1
	sti		r1, %:loop, %16
	sti     r1, %:live_wall, %1

wall:
	live	%53165
	st		r1, -10
	st		r1, -15
	st		r1, -24
	st		r1, -33
	st		r1, -42
	st		r1, -51
	st		r1, -60
	st		r1, -69
	st		r1, -78
	st		r1, -87
	st		r1, -96
	st		r1, -105
	st		r1, -114
	st		r1, -123
	st		r1, -132
	st		r1, -141
	st		r1, -150
	st		r1, -159
	st		r1, -168
	st		r1, -177
	st		r1, -186
	st		r1, -195
	st		r1, -204
	st		r1, -213
	st		r1, -222
	st		r1, -231
	st		r1, -240
	st		r1, -249
	st		r1, -258
	st		r1, -267
	st		r1, -276
	st		r1, -285
	st		r1, -294
	st		r1, -303
	st		r1, -312
	st		r1, -321
	st		r1, -330
	st		r1, -339
	st		r1, -348
	st		r1, -357
	st		r1, -366
	st		r1, -375
	st		r1, -384
	st		r1, -393
	st		r1, -402
	st		r1, -411
	st		r1, -420
	st		r1, -429
	st		r1, -438
	st		r1, -447
	zjmp	%:wall
	ld		%0, r2

loop:
	live	%654664
	ld		%0, r2
	fork	%:wall
	live	%354534
	fork	%:live_wall
	fork	%:live_wall2
	zjmp	%:loop

live_wall:
	live	%65468435
	ld		%0, r2
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	st 		r1, 126
	zjmp	%:live_wall

live_wall2:
	ld		%0, r2
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	live 	%1
	zjmp	%:live_wall2
