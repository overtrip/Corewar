# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tettouat <tettouat@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/10 12:13:51 by tettouat          #+#    #+#              #
#    Updated: 2017/02/10 12:17:35 by tettouat         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = YOLO

all: $(NAME)

$(NAME):
	@make -C libft/
	@make -C vm/
	@make -C comp_asm/

clean:
	@make -C libft/ clean
	@make -C vm/ clean
	@make -C comp_asm/ clean

fclean:
	@make -C libft/ fclean
	@make -C vm/ fclean
	@make -C comp_asm/ fclean

re: fclean all

.PHONY: all clean fclean re